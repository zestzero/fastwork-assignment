# Email sender
This project is a part of fastwork assignment.

## Getting Started
### Prerequisite

* [SendGrid](https://www.sendgrid.com/)
* [SparkPost API](https://developers.sparkpost.com/api/)
* [Nodejs](https://nodejs.org/en/)
* [Express and Express Router](https://expressjs.com/)
* [Reactjs](https://github.com/facebook/react) [Reactjs Blog](https://reactjs.org/)
* [React Semantic UI](https://react.semantic-ui.com/)
* [Get Start with Docker](https://docs.docker.com/get-started/)
* [RabbitMQ](https://www.rabbitmq.com/)
* [CloudAMQP](https://devcenter.heroku.com/articles/cloudamqp#use-with-node-js)

### Development

* Clone repository
```bash
$ git clone https://zestzero@bitbucket.org/zestzero/fastwork-assignment.git
```

* Start Services (MongoDB + RabbitMQ)
```bash
$ docker-compose up
```

Using [yarn](https://yarnpkg.com/):

* Install dependencies
``` bash
$ yarn
```
* Start Server
``` bash
$ yarn start
```
* Start Client
``` bash
$ cd client && yarn && yarn start
```

### Testing

* Server
``` bash
$ yarn test
```
* Client
``` bash
$ cd client && yarn test
```

### Deployment

Using [heroku container](https://devcenter.heroku.com/articles/container-registry-and-runtime) to deploy the container using Dockerfile.

``` bash
$ heroku container:login
$ heroku create
$ heroku container:push web
$ heroku container:release web
$ heroku open
```

### Documentation

* [SendGrid API Client](https://app.sendgrid.com/guide/integrate/langs/nodejs)
* [SparkPost Nodejs](https://github.com/SparkPost/node-sparkpost)
* [CloudAMQP](https://devcenter.heroku.com/articles/cloudamqp#use-with-node-js)
* [RabbitMQ](https://www.rabbitmq.com/tutorials/tutorial-two-javascript.html)
* [Using RabbitMQ for distributed work in Nodejs](https://medium.com/yld-engineering-blog/using-rabbitmq-and-amqp-for-distributed-work-queues-in-node-js-5247f735928)


### Future works

* Improve failover methodology to automatically retry sending email when both providers failed.
* Add JWT to verify incoming request using `access_token`.
* Add more unit test.
* Improve UI to be more user friendly.

## Requirements

* **Node.js** 8 or higher

## Contributing

Please check [CONTRIBUTING](CONTRIBUTING.md) before making a contribution.

## License

[Apache License Version 2.0](LICENSE)
