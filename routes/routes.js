const bodyParser = require('body-parser')
const EmailApis = require('./mail-api')

exports.init = (app) => {
  app.use(bodyParser.json())
  app.use('/api', EmailApis)
}
