const express = require('express')
const router = express.Router()
const Joi = require('joi')
const EmailController = require('../controllers/email')
const { apiErrorHandler } = require('../middlewares/error')

router.post('/sendmail', apiErrorHandler, async (req, res) => {
  const schema = Joi.object().keys({
    to_email: Joi.string().email().required(),
    from_email: Joi.string().email().required(),
    subject: Joi.string(),
    content: Joi.string(),
  })
  const { error } = Joi.validate(req.body, schema)
  if (error) return res.status(400).json(error.details)

  const channel = req.app.get('channel')
  const msg = {
    to: req.body.to_email,
    from: req.body.from_email,
    subject: req.body.subject,
    text: req.body.content,
  };
  const response = await EmailController.pubEmailQueue({ channel, data: msg })
  return res.json(response)
})

router.post('/email.get', apiErrorHandler, async (req, res) => {
  const schema = Joi.object().keys({
    from_email: Joi.string().email().required(),
  })
  const { error } = Joi.validate(req.body, schema)
  if (error) return res.status(400).json(error.details)
  const response = await EmailController.getEmailLogsByEmail({ fromEmail: req.body.from_email })
  return res.json(response)
})


module.exports = router
