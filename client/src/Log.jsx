import React from "react";
import { List } from "semantic-ui-react";

const Log = ({ index, fromEmail, toEmail, completed, provider }) => {
  return (
    <List.Item>
      <List.Content>
        <List.Header>{index + 1}</List.Header>
        {fromEmail} => {toEmail} {completed ? 'completed' : 'error'} by {provider}
      </List.Content>
    </List.Item>
  );
};

export { Log };
