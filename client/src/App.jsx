import "./App.css";

import _ from "lodash";
import React, { Component } from "react";
import { FormInput, SubmitButton } from "./common";
import { FormType } from "./common/constants";
import { sendEmail, getEmailLogs } from "./services/email";
import { Log } from "./Log";
import { Segment, Header, Icon, List } from "semantic-ui-react";

class App extends Component {
  state = {
    toEmail: "",
    fromEmail: "",
    subject: "",
    text: "",
    emailLogs: [],
    error: "",
    loading: false,
  };

  onInputChange = field => value => {
    this.setState({ [field]: value });
  };

  onFetchEmailLog = async () => {
    this.setState({ loading: true }, async () => {
      if (_.isEmpty(this.state.fromEmail)) return;
      const { error, result } = await getEmailLogs({
        email: this.state.fromEmail
      });
      this.setState({ loading: false, emailLogs: result, error });
    })
  };

  onSendEmail = async () => {
    const { toEmail, fromEmail, subject, content } = this.state;
    await sendEmail({ toEmail, fromEmail, subject, content });
  };

  renderLogs = () =>
    this.state.emailLogs ? (
      <List horizontal>
        {this.state.emailLogs.map((log, index) => (
          <Log key={`${log.toEmail}-${index}`} index={index} {...log} />
        ))}
      </List>
    ) : null;

  render() {
    return (
      <div className="App">
        <Header as="h2" icon textAlign="center">
          <Icon name="mail" circular />
          <Header.Content>EMAIL</Header.Content>
        </Header>
        <Segment.Group>
          <Segment>
            <FormInput
              className="Form-input"
              placeHolder="Sender email"
              type={FormType.Email}
              onInputChange={this.onInputChange("fromEmail")}
              onBlur={this.onFetchEmailLog}
              autoFocus
              loading={this.state.loading}
            />
          </Segment>
          <Segment>
            <FormInput
              className="Form-input"
              placeHolder="Recipient email"
              type={FormType.Email}
              onInputChange={this.onInputChange("toEmail")}
            />
          </Segment>
          <Segment>
            <FormInput
              className="Form-input"
              placeHolder="Subject"
              type={FormType.Text}
              onInputChange={this.onInputChange("subject")}
            />
          </Segment>
          <Segment>
            <FormInput
              className="Form-input"
              placeHolder="Content"
              type={FormType.Text}
              onInputChange={this.onInputChange("text")}
            />
          </Segment>
          <Segment>
            <SubmitButton onClick={this.onSendEmail}>SEND</SubmitButton>
          </Segment>
        </Segment.Group>
        {this.state.emailLogs.length > 0 && (
          <Segment.Group>
            <div className="card">{this.renderLogs()}</div>
          </Segment.Group>
        )}
      </div>
    );
  }
}

export default App;
