import axios from "axios";

const sendEmail = async ({ toEmail, fromEmail, subject, content }) => {
  try {
    const promise = await axios.post("/api/sendmail", {
      to_email: toEmail,
      from_email: fromEmail,
      subject,
      content
    });
    return { result: promise.data };
  } catch (error) {
    return { error }
  }
};

const getEmailLogs = async ({ email }) => {
  try {
    const promise = await axios.post("/api/email.get", { from_email: email });
    return { result: promise.data };
  } catch (error) {
    console.log(error.message)
    return { error }
  }
};

export { sendEmail, getEmailLogs };
