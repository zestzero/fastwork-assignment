import React, { Component } from "react";
import PropTypes from "prop-types";
import { Button } from 'semantic-ui-react'

class SubmitButton extends Component {
  static propTypes = {
    children: PropTypes.string,
    onClick: PropTypes.func.isRequired
  };

  render() {
    return <Button primary onClick={this.props.onClick}>{this.props.children}</Button>;
  }
}

export default SubmitButton;
