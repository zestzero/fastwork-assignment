import React, { Component } from "react";
import PropTypes from "prop-types";
import { Input } from "semantic-ui-react";

class FormInput extends Component {
  static propTypes = {
    type: PropTypes.string.isRequired,
    className: PropTypes.string,
    placeHolder: PropTypes.string,
    autoFocus: PropTypes.bool,
    loading: PropTypes.bool,
    onInputChange: PropTypes.func,
    onBlur: PropTypes.func
  };

  onInputChange = e => {
    this.props.onInputChange(e.target.value);
  };

  render() {
    return (
      <Input
        className={this.props.className}
        placeholder={this.props.placeHolder}
        type={this.props.type}
        onChange={this.onInputChange}
        onBlur={this.props.onBlur}
        autoFocus={this.props.autoFocus}
        loading={this.props.loading}
      >
        {this.props.children}
      </Input>
    );
  }
}

export default FormInput;
