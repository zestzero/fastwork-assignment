const SparkPost = require('sparkpost');
const { Queue, Providers } = require('./constants')
const { decode } = require('../utils/utils')
const { createEmailLog } = require('../models/email')

const client = new SparkPost(process.env.SPARKPOST_API_KEY);

async function sendMessage({ msg }) {
  client.transmissions.send({
    options: {
      sandbox: true
    },
    content: {
      from: msg.from,
      subject: msg.subject,
      html: msg.text
    },
    recipients: [
      {address: msg.to}
    ]
  })
  .then(data => {
    console.log('Woohoo! You just sent your first mailing!');
    return { result: data }
  })
  .catch(err => {
    console.log('Failed sending email via sparkpost!')
    console.error(err)
    return { err }
  })
}

async function logEmailHistory({ message, completed, provider }) {
  return createEmailLog({
    fromEmail: message.from_email,
    toEmail: message.to_email,
    completed,
    provider
  })
}

async function consume({ channel }) {
  channel.consume(Queue.EmailQueueSecondary, async (msg) => {
    if (msg !== null) {
      const message = decode(msg.content)
      const { error } = await sendMessage({ msg: message })
      if (error) {
        console.error('Sending email error')
        await logEmailHistory({ message, completed: false, provider: Providers.SparkPost })
        return
      }
      await logEmailHistory({ message, completed: true, provider: Providers.SparkPost })
      channel.ack(msg)
    }
  })
}

exports.init = async ({ channel }) => {
  channel.get(Queue.EmailQueue, {}, (err, msg) => {
    if (err) {
      console.warn(err.message)
    } else if (msg) {
      setTimeout(async () => {
        channel.ack(msg)
        await consume({ channel })
      }, 1e3)
    } else {
      setTimeout(async () => await consume({ channel }), 1e3)
    }
  })
}