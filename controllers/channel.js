const url = process.env.CLOUDAMQP_URL || 'amqp://guest:guest@localhost:5672';
const amqp = require("amqplib/callback_api")

exports.createQueueChannel = async (queue, callback) => {
  return amqp.connect(url, async (err, conn) => {
    if (err) callback(err)
    console.log('RabbitMQ connected!')
    await conn.createChannel(async (err, channel) => {
      if (err) callback(err)
      console.log('Channel created!')
      await channel.assertQueue(queue, { durable: true }, (err) => {
        if (err) callback(err)
        callback(null, channel, conn)
      })
    })
  })
}