const { Queue } = require('./constants')
const { encode } = require('../utils/utils')
const { getLogsByFromEmail } = require('../models/email')

exports.pubEmailQueue = ({ channel, data }) => {
  channel.assertQueue(Queue.EmailQueue);
  channel.sendToQueue(Queue.EmailQueue, encode(data), {
    persistent: true
  });
}

exports.getEmailLogsByEmail = async ({ fromEmail }) => {
  return getLogsByFromEmail({ fromEmail })
}