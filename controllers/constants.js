exports.Queue = {
  EmailQueue: 'email_queue',
  EmailQueueSecondary: 'email_queue_secondary'
}

exports.Providers = {
  SendGrid: 'sendgrid',
  SparkPost: 'sparkpost'
}