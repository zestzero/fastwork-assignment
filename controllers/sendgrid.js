const sgMail = require("@sendgrid/mail")
const { Queue, Providers } = require('./constants')
const { decode } = require('../utils/utils')
const { createEmailLog } = require('../models/email')

sgMail.setApiKey(process.env.SENDGRID_API_KEY)

async function sendMessage({ msg }) {
  try{
    await sgMail.send(msg, true, (error, res) => {
    if (error) {
      console.error('Failed sending email via sendgrid!')
      return { error }
    }
    return { result: res }
  })
  } catch(error) {
    return { error }
  }
}

async function logEmailHistory({ message, completed, provider }) {
  return createEmailLog({
    fromEmail: message.from,
    toEmail: message.to,
    completed,
    provider
  })
}

async function consume({ channel }) {
  channel.consume(Queue.EmailQueue, async (msg) => {
    if (msg !== null) {
      const message = decode(msg.content)
      const { error } = await sendMessage({ msg: message })
      if (error) {
        console.log('Resent to queue')
        await logEmailHistory({ message, completed: false, provider: Providers.SendGrid })
        channel.sendToQueue(Queue.EmailQueueSecondary, msg.content, { persistent: true })
        return
      }
      await logEmailHistory({ message, completed: true, provider: Providers.SendGrid })
      channel.ack(msg)
    }
  })
}

exports.init = async ({ channel }) => {
  channel.get(Queue.EmailQueue, {}, (err, msg) => {
    if (err) {
      console.warn(err.message)
    } else if (msg) {
      setTimeout(async () => {
        channel.ack(msg)
        await consume({ channel })
      }, 1e3)
    } else {
      setTimeout(async () => await consume({ channel }), 1e3)
    }
  })
}