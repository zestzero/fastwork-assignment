exports.toObject = (email) => {
  return {
    id: email._id,
    fromEmail: email.from_email,
    toEmail: email.to_email,
    completed: email.completed,
    provider: email.provider
  }
}