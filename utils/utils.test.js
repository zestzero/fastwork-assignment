const chai = require('chai')
const expect = chai.expect
const { encode, decode } = require('./utils')

const mockMessage = {
  to_email: 'toEmail@test.com',
  from_email: 'fromEmail@test.com'
}

describe('encode', () => {
  it('should return encoded json', () => {
    const actual = encode(mockMessage)
    expect(typeof actual).to.equal('object')
    expect(actual[0]).to.equal(123)
  })
})

describe('decode', () => {
  it('should return decode buffer', () => {
    const encoded = encode(mockMessage)
    const result = decode(encoded)
    expect(result).to.deep.equal(mockMessage)
  })
})