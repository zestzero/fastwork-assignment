exports.encode = (doc) => {
  return Buffer.from(JSON.stringify(doc))
}

exports.decode = (data) => {
  const result = Buffer.from(data).toString('ascii')
  return JSON.parse(result)
}
