require('dotenv').config()

const express = require('express')
const path = require('path')
const Routes = require('./routes/routes')
const MongoController = require('./controllers/mongo')
const Channel = require('./controllers/channel')
const { Queue } = require('./controllers/constants')
const SendGridController = require('./controllers/sendgrid')

const app = express()
app.use(express.static(path.join(__dirname, 'client/build')))

MongoController.init({ mongodbUri: process.env.MONGODB_URI })(() => {
  Channel.createQueueChannel(Queue.EmailQueue, function(err, channel, conn) {
    if (err) {
      console.error(err.stack);
    }
    else {
      console.log(`'${Queue.EmailQueue}' channel and queue created`);
      SendGridController.init({ channel })
      app.set('channel', channel)
      Routes.init(app)

      app.get('/', (req, res) => {
        res.sendFile(path.join(__dirname, 'client/build', 'index.html'))
      })
    }
  })
})

const server = app.listen(process.env.PORT || 5000, () => {
  const port = server.address().port
  console.log('Server start at http://localhost:%s', port)
})
