const chai = require("chai")
const expect = chai.expect
const EmailModel = require("./email")
const { dbUp, reset, dbDown } = require("../utils/db-helper")

describe("EmailModel tests", () => {
  before(dbUp)
  beforeEach(reset)

  describe("createEmailLog", () => {
    it("should create a log and return created log correctly", async () => {
      const log = await EmailModel.createEmailLog({
        fromEmail: "from@gg.com",
        toEmail: "to@gg.com",
        completed: true,
        provider: "sendgrid"
      })
      const actualLog = await EmailModel.getLogById({ logId: log.id })
      expect(actualLog.fromEmail).to.equal("from@gg.com")
      expect(actualLog.toEmail).to.equal("to@gg.com")
      expect(actualLog.provider).to.equal("sendgrid")
    })
  })

  describe("updateEmailLog", () => {
    it("should update a log and return updated log correctly", async () => {
      const log = await EmailModel.createEmailLog({
        fromEmail: "from@gg.com",
        toEmail: "to@gg.com",
        completed: false,
        provider: "sendgrid"
      })
      await EmailModel.updateEmailLog({
        fromEmail: log.fromEmail,
        toEmail: log.toEmail,
        provider: log.provider,
        completed: true
      })
      const actualLog = await EmailModel.getLogById({ logId: log.id })
      expect(actualLog.completed).to.equal(true)
    })
  })

  describe("getLogsByFromEmail", () => {
    it("should return created logs correctly", async () => {
      await EmailModel.createEmailLog({
        fromEmail: "from@gg.com",
        toEmail: "to1@gg.com",
        completed: false,
        provider: "sendgrid"
      })

      await EmailModel.createEmailLog({
        fromEmail: "from@gg.com",
        toEmail: "to2@gg.com",
        completed: false,
        provider: "sendgrid"
      })
      const actualLog = await EmailModel.getLogsByFromEmail({ fromEmail: "from@gg.com" })
      expect(actualLog.length).to.equal(2)
      expect(actualLog[0].toEmail).to.equal("to1@gg.com")
      expect(actualLog[1].toEmail).to.equal("to2@gg.com")
    })
  })

  after(dbDown)
})
