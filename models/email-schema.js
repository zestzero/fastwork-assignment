const mongoose = require("mongoose");

const EmailSchema = new mongoose.Schema({
  to_email: {
    type: String,
    required: true,
    index: true
  },
  from_email: {
    type: String,
    required: true,
    index: true
  },
  completed: {
    type: Boolean,
    default: false
  },
  provider: {
    type: String
  },
  create_date: {
    type: Date,
    default: new Date()
  },
  update_date: {
    type: Date,
    default: new Date()
  },
});

const Email = mongoose.model("Email", EmailSchema);
module.exports = Email;
