const EmailSchema = require("./email-schema");
const { toObject } = require('../utils/email')

exports.createEmailLog = async ({
  fromEmail,
  toEmail,
  completed,
  provider
}) => {
  const log = await EmailSchema.create({
    from_email: fromEmail,
    to_email: toEmail,
    completed,
    provider,
  })

  return toObject(log)
}

exports.updateEmailLog = async ({ fromEmail, toEmail, completed, provider }) => {
  const log = await EmailSchema.findOneAndUpdate({
    from_email: fromEmail,
    to_email: toEmail,
    provider,
  }, {
    completed,
    update_date: new Date()
  }, {
    new: true
  })

  return toObject(log)
}

exports.getLogsByFromEmail = async({ fromEmail }) => {
  const logs = await EmailSchema.find({
    from_email: fromEmail
  })

  return logs.map(toObject)
}

exports.getLogById = async ({ logId }) => {
  const log = await EmailSchema.findById(logId)
  return toObject(log)
}